package ru.t1consulting.vmironova.tm.exception.user;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email already exists.");
    }

}

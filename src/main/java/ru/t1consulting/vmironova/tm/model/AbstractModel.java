package ru.t1consulting.vmironova.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
public abstract class AbstractModel implements Serializable {

    @NotNull
    private String id = UUID.randomUUID().toString();

}

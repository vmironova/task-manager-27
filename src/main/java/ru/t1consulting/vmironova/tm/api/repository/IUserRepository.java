package ru.t1consulting.vmironova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password);

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email);

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    Boolean isLoginExists(@Nullable String login);

    Boolean isEmailExists(@Nullable String email);

}

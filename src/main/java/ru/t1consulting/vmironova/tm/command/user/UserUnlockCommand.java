package ru.t1consulting.vmironova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "User unlock.";

    @NotNull
    public static final String NAME = "user-unlock";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

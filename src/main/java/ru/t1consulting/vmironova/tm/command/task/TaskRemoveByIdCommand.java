package ru.t1consulting.vmironova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.vmironova.tm.model.Task;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove task by id.";

    @NotNull
    public static final String NAME = "task-remove-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @Nullable final Task task = getTaskService().findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        getTaskService().removeById(userId, task.getId());
    }

}
